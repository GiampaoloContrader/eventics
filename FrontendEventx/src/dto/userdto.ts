//Se nel backend è diverso, cambia i campi

export class UserDTO{ 
    
    id: number;

    nome: string;

    cognome: string;

    email: string;
    
    password: string;

    immagine: string;
    
    eventi_seguiti:boolean;

    nuovi_eventi:boolean;

    espositori:boolean;

    organizzatori:boolean;
}