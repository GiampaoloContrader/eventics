export class ImmaginiDTO{
    id:number;
    isSponsor:boolean;
    immagine:Blob;
    type:string;
    size:string;
}