import { UserDTO } from 'src/dto/userdto';
import { TokenStorageService } from './../_services/token-storage.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: UserDTO;
  authenticated = false;
  tokenService: TokenStorageService = new TokenStorageService();


  constructor(tokenService: TokenStorageService) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('auth-user')) {
      this.user = JSON.parse(sessionStorage.getItem('auth-user'));
      this.authenticated = true;
      console.log(JSON.parse(sessionStorage.getItem('auth-user')));

    }

  }

  logout(): void {
    this.tokenService.signOut();
    window.location.reload();
  }

}


