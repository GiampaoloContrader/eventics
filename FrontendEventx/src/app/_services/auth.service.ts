// Questo servizio invia richieste HTTP POST di registrazione e di accesso al back-end.
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserDTO } from 'src/dto/userdto';

const AUTH_API = 'http://localhost:8080/micro-event/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    console.log(environment.apiUrl);
    return this.http.post(environment.apiUrl + '/micro-event/user/login', {
      "email": email,
      "password": password
    }, httpOptions);
  }

  register(userDTO: UserDTO): Observable<any> {
    return this.http.post(environment.apiUrl + '/micro-event/user/register', userDTO, httpOptions);
  }

  loginpro(email: string, password: string): Observable<any> {
    console.log(email, password + 'porcoddio');
    console.log(environment.apiUrl);
    return this.http.post(environment.apiUrl + '/micro-event/userpro/login', {
      "email": email,
      "password": password
    }, httpOptions);
  }


}
