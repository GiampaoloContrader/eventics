import { environment } from './../../environments/environment';
/*Questo servizio fornisce metodi per accedere a risorse pubbliche e protette.
Lo faccio semplice perché abbiamo HttpInterceptor.*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from 'src/dto/userdto';
import { AbstractService } from './abstractservice';

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root'
})
export class UserService extends AbstractService<UserDTO>{


  loggeduser:UserDTO = null;

  constructor( http: HttpClient) {
    super(http)
    this.type= 'user';
   }


  setCurrentUser(user:UserDTO){
    this.loggeduser = user;
    sessionStorage.setItem("currentUser", JSON.stringify(user));
  }

  getPublicContent(): Observable<any> {
    return this.http.get(environment.apiUrl + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(environment.apiUrl + 'user', { responseType: 'text' });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(environment.apiUrl + 'mod', { responseType: 'text' });
  }

  getOrganizzazioneBoard(): Observable<any> {
    return this.http.get(environment.apiUrl + 'organizzatore', { responseType: 'text' });
  }

  getEspositoreBoard(): Observable<any> {
    return this.http.get(environment.apiUrl + 'espositore', { responseType: 'text' });
  }




}
