/*HttpInterceptor ha un intercept()metodo per ispezionare e trasformare le richieste HTTP prima che vengano inviate al server.
AuthInterceptor implemente HttpInterceptor.
 Aggiungeremo l' intestazione di autorizzazione con il prefisso "Bearer" al token.*/
 import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
 import { Injectable } from '@angular/core';
 import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
 
 import { TokenStorageService } from '../_services/token-storage.service';
 import { Observable } from 'rxjs';
 
 const TOKEN_HEADER_KEY = 'Authorization';       // per Spring Boot back-end
 
 @Injectable()
 export class AuthInterceptor implements HttpInterceptor {
   constructor(private token: TokenStorageService) { }
 
   /*intercept() ottiene l'oggetto HTTPRequest , lo cambia e passa HttpHandler al handle() ovvero il metodo dell'oggetto . 
   trasforma HTTPRequest in un Observable<HttpEvents>.
   next: HttpHandler object rappresenta il prossimo intercettore nella catena degli intercettori.
   Il "prossimo" finale nella catena è Angular HttpClient .*/
   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     let authReq = req;
     const token = this.token.getToken();
     if (token != null) {
       authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
     }
     return next.handle(authReq);
   }
 }
 
 export const authInterceptorProviders = [
   { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
 ];