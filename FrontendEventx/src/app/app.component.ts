import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[] = ['ROLE_USER'];
  isLoggedIn = false;
  showUserBoard = false;
  showOrganizzatoreBoard = false;
  showModeratorBoard = false;
  showEspositoreBoard= false;
  username?: string;

  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      console.log (user.roles);
      this.showUserBoard = this.roles.includes('ROLE_USER');
      this.showOrganizzatoreBoard = this.roles.includes('ROLE_ORGANIZZATORE');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showEspositoreBoard = this.roles.includes('ROLE_ESPOSITORE');


      this.username = user.username;
    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
