import { BoardEspositoreComponent } from './board-espositore/board-espositore.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardOrganizzazioneComponent } from './board-organizzazione/board-organizzazione.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SPIDComponent } from './spid/spid.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { EventoDettagliatoComponent } from './evento-dettagliato/evento-dettagliato.component';
import { MappaComponent } from './mappa/mappa.component';
import { ListaEventiComponent } from './lista-eventi/lista-eventi.component';
import { OrganizzatoreCreazioneEventoComponent } from './organizzatore-creazione-evento/organizzatore-creazione-evento.component';




import { LoginComponent } from './login/login.component';
import { LoginProComponent } from './login-pro/login-pro.component';


const routes: Routes = [
  { path: '', redirectTo: 'land', pathMatch: 'full' },
  { path: 'home', component: HomepageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'login-pro', component: LoginProComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'organizzazione', component: BoardOrganizzazioneComponent },
  { path: 'espositore', component: BoardEspositoreComponent},
  { path: 'footer', component :FooterComponent},
  { path: 'header', component :HeaderComponent},
  { path: 'spid', component: SPIDComponent },
  { path: 'land', component: LandingPageComponent},
  { path: 'calendario', component: CalendarioComponent},
  { path: 'dettaglievento', component: EventoDettagliatoComponent},
  { path: 'mappa', component: MappaComponent},
  { path: 'listaeventi', component: ListaEventiComponent},
  { path: 'creazioneEvento', component: OrganizzatoreCreazioneEventoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
