import { Router } from '@angular/router';
import { TokenStorageService } from './../_services/token-storage.service';
import { Component, OnInit } from '@angular/core';
import { UserDTO } from 'src/dto/userdto';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
//services
tokenService: TokenStorageService = new TokenStorageService;

 //Controllo password
vecchia_password:string;
ripetizione_password:string;
//Controllo per gli errori
check_campi:boolean;
check_password_attuale: boolean;
check_password: boolean;
//DTO
user: UserDTO[];
usertoinsert: UserDTO = new UserDTO();
  constructor(private service : UserService, private router: Router) { }

  ngOnInit(): void {
    this.usertoinsert = JSON.parse(localStorage.getItem('currentUser'));
  }


//Aggiorna i campi dell'utente
UpdateUser(user: UserDTO){
  this.check_campi = false;
  this.check_password_attuale= false;
  this.check_password=false;
  //Check per verificare che tutti i campi siano riempiti
 if (this.usertoinsert.nome == null || this.usertoinsert.cognome == null || this.usertoinsert.email == null || this.usertoinsert.password == null || this.ripetizione_password == null || this.vecchia_password == null){
    this.check_campi= true;
    this.check_password_attuale = false;
    this.check_password=false;
  } //check per controllare che la vecchia password sia esatta
  else if (this.vecchia_password != this.usertoinsert.password){
    this.check_campi= true;
    this.check_password_attuale = true;
    this.check_password=false;
  }//check per controllare che la password sia uguale alla sua ripetizione
   else if (this.usertoinsert.password != this.ripetizione_password){
    this.check_campi= true;
    this.check_password_attuale = false;
    this.check_password=true;
  }else{
    //this.service.UpdateUser(this.nome, this.cognome, this.email, this.password, this.immagine, this.eventi_seguiti, this.nuovi_eventi, this.espositori, this.organizzatori).subscribe (user => this.user = user);
    this.service.update(user).subscribe (user => this.usertoinsert = user);
  }
}


  logout(): void {
    this.tokenService.signOut();
    this.router.navigate(['/home']);
  }

}
