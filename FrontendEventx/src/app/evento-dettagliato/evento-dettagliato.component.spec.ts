import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoDettagliatoComponent } from './evento-dettagliato.component';

describe('EventoDettagliatoComponent', () => {
  let component: EventoDettagliatoComponent;
  let fixture: ComponentFixture<EventoDettagliatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventoDettagliatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoDettagliatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
