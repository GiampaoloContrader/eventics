/*Questi componenti sono basati sui ruoli. Ma l'autorizzazione verrà elaborata dal back-end.
Abbiamo solo bisogno di chiamare UserServicemetodi:
getUserBoard ()
getModeratorBoard ()
getOrganizzatoreBoard ()*/
import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-organizzazione',
  templateUrl: './board-organizzazione.component.html',
  styleUrls: ['./board-organizzazione.component.css']
})
export class BoardOrganizzazioneComponent implements OnInit {
  content?: string;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getOrganizzazioneBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
}