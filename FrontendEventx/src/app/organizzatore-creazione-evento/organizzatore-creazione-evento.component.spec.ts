import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizzatoreCreazioneEventoComponent } from './organizzatore-creazione-evento.component';

describe('OrganizzatoreCreazioneEventoComponent', () => {
  let component: OrganizzatoreCreazioneEventoComponent;
  let fixture: ComponentFixture<OrganizzatoreCreazioneEventoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizzatoreCreazioneEventoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizzatoreCreazioneEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
