
// IMPORT MODULI


import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// IMPORT COMPONENTI

import { CarouselComponent } from './homepage/carousel/carousel.component';
import { EventoComponent } from './homepage/evento/evento.component';
import { LoginComponent } from './login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardOrganizzazioneComponent } from './board-organizzazione/board-organizzazione.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardEspositoreComponent } from './board-espositore/board-espositore.component';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SPIDComponent } from './spid/spid.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { EventoDettagliatoComponent } from './evento-dettagliato/evento-dettagliato.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { MappaComponent } from './mappa/mappa.component';
import { LoginProComponent } from './login-pro/login-pro.component';
import { ListaEventiComponent } from './lista-eventi/lista-eventi.component';
import { OrganizzatoreCreazioneEventoComponent } from './organizzatore-creazione-evento/organizzatore-creazione-evento.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomepageComponent,
    ProfileComponent,
    BoardOrganizzazioneComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    BoardEspositoreComponent,
    CarouselComponent,
    EventoComponent,

      HeaderComponent,
      FooterComponent,
      SPIDComponent,
      LandingPageComponent,
      EventoDettagliatoComponent,
      CalendarioComponent,
      MappaComponent,
      LoginProComponent,
      ListaEventiComponent,
      OrganizzatoreCreazioneEventoComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
