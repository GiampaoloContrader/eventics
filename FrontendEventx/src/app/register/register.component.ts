/*questo componente binds il form data (username, email, password) dal template al metodo
 AuthService.register() che ritornerà un Observable object*/
import { Component, OnInit } from '@angular/core';
import { UserDTO } from 'src/dto/userdto';
import { AuthService } from '../_services/auth.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: any = {
    nome: null,
    cognome: null,
    email: null,
    password: null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  conferma_password;
//DTO
  user: UserDTO[];
  usertoinsert: UserDTO = new UserDTO();

  constructor(private authService: AuthService, private service: UserService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {

    this.authService.register(this.usertoinsert).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
  insert(user: UserDTO){
    this.service.insert(user).subscribe();
  }
}
