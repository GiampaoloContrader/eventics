import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  constructor() { }

  sponsoredEvents=[
    {
      "img_src":"../../../assets/carousel/Immagine1.jpg",
      "product_name":"Demo1",
      "productsortdisc": "this is caroisel discription"
    },
    {
      "img_src":"../../../assets/carousel/Immagine22.jpg",
      "product_name":"Demo2",
      "productsortdisc": "this is caroisel discription"
    },
    {
      "img_src":"../../../assets/carousel/Immagine33.jpg",
      "product_name":"Demo3",
      "productsortdisc": "this is caroisel discription"
    }
  ];

  ngOnInit(): void {
  }

}

