import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {

  constructor() { }


  eventi = [
    {
      "img_src":"../../../assets/carousel/Immagine1.jpg",
      "event_name":"Sagra della porchetta",
      "event_location": "Ariccia(RM) - Lazio",
      "index": "1"
    },
    {
      "img_src":"../../../assets/carousel/Immagine22.jpg",
      "event_name":"Mercatini di Natale",
      "event_location": "Cortina d'Ampezzo(BL) - Veneto",
      "index": "2"
    },
    {
      "img_src":"../../../assets/carousel/Immagine33.jpg",
      "event_name":"NNDR - Nel Nome Del Rock",
      "event_location": "Palestrina(RM) - Lazio",
      "index": "3"
    },
    {
      "img_src":"../../../assets/carousel/Immagine33.jpg",
      "event_name":"LE COSE",
      "event_location": "Ndo te pare",
      "index": "4"
    },
    {
      "img_src":"../../../assets/carousel/Immagine33.jpg",
      "event_name":"LE ALTRE COSE",
      "event_location": "Sempre ndo te pare",
      "index": "5"
    }
  ];

  ngOnInit(): void {
  }

}
