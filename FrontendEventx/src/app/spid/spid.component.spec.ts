import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SPIDComponent } from './spid.component';

describe('SPIDComponent', () => {
  let component: SPIDComponent;
  let fixture: ComponentFixture<SPIDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SPIDComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SPIDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
