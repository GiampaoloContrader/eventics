package it.contrader.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.contrader.dto.UserDTO;
import it.contrader.dto.UserProDTO;
import it.contrader.security.UsernamePasswordAuthentication;
import it.contrader.util.JwtUtil;

@Component
public class InitialAuthenticationFilter extends OncePerRequestFilter {
	@Autowired
	private AuthenticationManager authManager;

	@Autowired
	private JwtUtil jwtUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		UserDTO login = new ObjectMapper().readValue(request.getInputStream(), UserDTO.class);

		UsernamePasswordAuthentication a = new UsernamePasswordAuthentication(login.getEmail(), login.getPassword());
		authManager.authenticate(a);

		response.setHeader("Authorization", jwtUtil.generateJwtToken(login.getEmail()));
		
		
		UserProDTO loginP = new ObjectMapper().readValue(request.getInputStream(), UserProDTO.class);

		UsernamePasswordAuthentication b = new UsernamePasswordAuthentication(loginP.getEmail(), loginP.getPassword());
		authManager.authenticate(b);

		response.setHeader("Authorization", jwtUtil.generateJwtToken(login.getEmail()));
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return !request.getServletPath().equals("/micro-event/login");
	}

}
