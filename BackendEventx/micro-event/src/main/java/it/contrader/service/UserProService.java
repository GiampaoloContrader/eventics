package it.contrader.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.contrader.converter.UserProConverter;
import it.contrader.dto.UserProDTO;
import it.contrader.model.UserPro;
import it.contrader.model.UserPro.UserType;
import it.contrader.repository.UserProRepository;

@Service
public class UserProService extends AbstractService<UserPro, UserProDTO> {
	
	@Autowired
	private UserProConverter userProConverter;

	@Autowired
	private UserProRepository userProRepository;
	

	

	public UserProDTO findUserProByEmailAndPassword(String email, String password) {
		return userProConverter.toDTO( userProRepository.findUserProByEmailAndPassword(email, password) );
	}
	
	public UserProDTO findUserProByUserType(UserType userType) {
		return userProConverter.toDTO( userProRepository.findUserProByUserType(userType));
	}
	
	public UserProDTO findUserProByEmail(String email) {
		return userProConverter.toDTO( userProRepository.findUserProByEmail(email));
	}

	public UserProDTO findUserProByNomeAndCognome(String nome, String cognome) {
		return userProConverter.toDTO( userProRepository.findUserProByNomeAndCognome(nome, cognome));
	}
	
	public UserProDTO findUserProById(Long id) {
		return userProConverter.toDTO( userProRepository.findUserProById(id));
	}
	
}


