package it.contrader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.contrader.converter.OrganizzazioneConverter;
import it.contrader.dto.EventoDTO;
import it.contrader.dto.OrganizzazioneDTO;
import it.contrader.dto.UserProDTO;
import it.contrader.model.Evento;
import it.contrader.model.Organizzazione;
import it.contrader.model.UserPro;
import it.contrader.repository.OrganizzazioneRepository;


@Service
public class OrganizzazioneService extends AbstractService<Organizzazione,OrganizzazioneDTO> {
	
	@Autowired
	private OrganizzazioneConverter organizzazioneConverter;
	
	@Autowired
	private OrganizzazioneRepository organizzazioneRepository;
	
	//public OrganizzazioneDTO findOrganizzazioneByIdEvento(EventoDTO idEvento) {
	public OrganizzazioneDTO findOrganizzazioneByIdEvento(Evento idEvento) {	
		//return organizzazioneConverter.toDTO(organizzazioneRepository.findOrganizzazioneByIdEventoDTO(idEvento));
		return organizzazioneConverter.toDTO(organizzazioneRepository.findOrganizzazioneByIdEvento(idEvento));
	}
	
	//public OrganizzazioneDTO findOrganizzazioneByIdUserPro(UserProDTO idUserPro) {
	public OrganizzazioneDTO findOrganizzazioneByIdUserPro(UserPro idUserPro) {	
		//return organizzazioneConverter.toDTO(organizzazioneRepository.findOrganizzazioneByIdUserProDTO(idUserPro));
		return organizzazioneConverter.toDTO(organizzazioneRepository.findOrganizzazioneByIdUserPro(idUserPro));
	}

}
