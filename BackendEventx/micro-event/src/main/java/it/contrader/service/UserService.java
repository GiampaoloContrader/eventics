package it.contrader.service;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.contrader.converter.UserConverter;

import it.contrader.dto.UserDTO;
import it.contrader.model.User;
import it.contrader.repository.UserRepository;

@Service
public class UserService extends AbstractService<User, UserDTO> {
	@Autowired
	private UserConverter userConverter;

	@Autowired
	private UserRepository userRepository;
		

	public UserDTO findUserByEmailAndPassword(String email, String password) {
		return userConverter.toDTO( userRepository.findUserByEmailAndPassword(email, password) );
	}

	public UserDTO findUserByEmail(String email) {
		return userConverter.toDTO( userRepository.findUserByEmail(email) );
	}
	
	public UserDTO findUserByNomeAndCognome(String nome, String cognome) {
		return userConverter.toDTO( userRepository.findUserByNomeAndCognome(nome, cognome) );
	}
	
	public UserDTO findUserById(Long id) {
		return userConverter.toDTO( userRepository.findUserById(id) );
	}
	
}
