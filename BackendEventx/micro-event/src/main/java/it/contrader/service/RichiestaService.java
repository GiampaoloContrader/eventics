package it.contrader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.contrader.converter.RichiestaConverter;
import it.contrader.dto.RichiestaDTO;
import it.contrader.model.Evento;
import it.contrader.model.Richiesta;
import it.contrader.model.Richiesta.Stato;
import it.contrader.model.UserPro;
import it.contrader.repository.RichiestaRepository;


@Service
public class RichiestaService extends AbstractService<Richiesta,RichiestaDTO> {
	
	@Autowired
	private RichiestaConverter richiestaConverter;
	
	@Autowired
	private RichiestaRepository richiestaRepository;
	
	public RichiestaDTO findRichiestaByIdEvento(Evento idEvento) {
		
		return richiestaConverter.toDTO(richiestaRepository.findRichiestaByIdEvento(idEvento));
	}
	
	public RichiestaDTO findRichiestaByIdUserPro(UserPro idUserPro) {
		
		return richiestaConverter.toDTO(richiestaRepository.findRichiestaByIdUserPro(idUserPro));
	}
	
	public RichiestaDTO findRichiestaByStato(Stato stato) {
		
		return richiestaConverter.toDTO(richiestaRepository.findRichiestaByStato(stato));
	}

}
