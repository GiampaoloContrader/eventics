package it.contrader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.contrader.converter.EventoConverter;
import it.contrader.dto.EventoDTO;
import it.contrader.model.Evento;
import it.contrader.repository.EventoRepository;
import java.time.LocalDateTime;

@Service
public class EventoService extends AbstractService<Evento, EventoDTO> {
	@Autowired
	private EventoConverter eventoConverter;

	@Autowired
	private EventoRepository eventoRepository;

	public EventoDTO findEventoByNomeEvento(String nomeEvento) {
		return eventoConverter.toDTO(eventoRepository.findEventoByNomeEvento(nomeEvento));
	}

	public EventoDTO findEventoByTipo(String tipo) {
		return eventoConverter.toDTO(eventoRepository.findEventoByTipo(tipo));
	}

	public EventoDTO findEventoByLuogo(String luogo) {
		return eventoConverter.toDTO(eventoRepository.findEventoByLuogo(luogo));
	}

	public EventoDTO findEventoByDataInizio(LocalDateTime dataInizio) {
		return eventoConverter.toDTO(eventoRepository.findEventoByDataInizio(dataInizio));
	}

	public EventoDTO findEventoBySponsorizzato(Boolean sponsorizzato) {
		return eventoConverter.toDTO(eventoRepository.findEventoBySponsorizzato(sponsorizzato));
	}
	
	public EventoDTO findEventoByLuogoAndNomeEvento(String luogo,String nomeEvento) {
		return eventoConverter.toDTO(eventoRepository.findEventoByLuogoAndNomeEvento(luogo, nomeEvento));
	}
	
	public EventoDTO findEventoById(Long id) {
		return eventoConverter.toDTO(eventoRepository.findEventoById(id));
	}

}
