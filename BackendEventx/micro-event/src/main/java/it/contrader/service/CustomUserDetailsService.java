package it.contrader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import it.contrader.model.User;
import it.contrader.model.UserPro;
import it.contrader.model.UserProSecurity;
import it.contrader.model.UserSecurity;
import it.contrader.repository.UserProRepository;
import it.contrader.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserProRepository userProRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findUserByEmail(username);

		if (user != null) {
			return new UserSecurity(user);
		}

		UserPro userPro = userProRepository.findUserProByEmail(username);

		if (userPro != null) {
			return new UserProSecurity(userPro);
			
		} else {
			throw new UsernameNotFoundException(username);
		}

		

	}

}
