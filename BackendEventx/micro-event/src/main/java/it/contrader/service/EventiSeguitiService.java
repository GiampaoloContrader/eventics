package it.contrader.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.contrader.converter.EventiSeguitiConverter;
import it.contrader.dto.EventiSeguitiDTO;

import it.contrader.model.EventiSeguiti;
import it.contrader.repository.EventiSeguitiRepository;

@Service
public class EventiSeguitiService extends AbstractService<EventiSeguiti, EventiSeguitiDTO> { 
	@Autowired
	private EventiSeguitiConverter eventiSeguitiConverter;

	@Autowired
	private EventiSeguitiRepository eventiseguitiRepository;
	
	public EventiSeguitiDTO findEventoById(Long id) {
		return eventiSeguitiConverter.toDTO(eventiseguitiRepository.findEventiSeguitiById(id));
	}

	
}
