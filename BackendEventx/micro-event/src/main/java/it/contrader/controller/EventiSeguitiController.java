package it.contrader.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.contrader.dto.EventiSeguitiDTO;

@RestController
@RequestMapping("/micro-event/eventiseguiti")
public class EventiSeguitiController extends AbstractController<EventiSeguitiDTO> {

}
