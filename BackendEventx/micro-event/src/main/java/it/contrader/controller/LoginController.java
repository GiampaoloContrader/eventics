package it.contrader.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.contrader.dto.JwtResponseDTO;
import it.contrader.dto.LoginDTO;
import it.contrader.dto.UserDTO;
import it.contrader.dto.UserProDTO;
import it.contrader.security.UsernamePasswordAuthentication;
import it.contrader.util.JwtUtil;

@RestController
@RequestMapping("/micro-event")
public class LoginController {

	@Autowired
	private AuthenticationManager authManager;

	@Autowired
	private JwtUtil jwtUtil;

	@PostMapping("/user/login")
	public JwtResponseDTO login(@RequestBody LoginDTO login) {
		UsernamePasswordAuthentication a = new UsernamePasswordAuthentication(login.getEmail(), login.getPassword());
		authManager.authenticate(a);

		return new JwtResponseDTO(jwtUtil.generateJwtToken(login.getEmail()), login.getEmail());
	}

	@GetMapping("/user/refreshtoken")
	public JwtResponseDTO refreshtoken(HttpServletRequest request) {
		String jwt = "";
		String headerAuth = request.getHeader("Authorization");

		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
			jwt = headerAuth.substring(7, headerAuth.length());
		}

		String email = jwtUtil.parseUsername(jwt);
		return new JwtResponseDTO(jwtUtil.generateRefreshJwtToken(email), email);
	}

	@PostMapping("/userpro/login")
	public JwtResponseDTO loginPro(@RequestBody LoginDTO login) {
		UsernamePasswordAuthentication b = new UsernamePasswordAuthentication(login.getEmail(), login.getPassword());
		authManager.authenticate(b);

		return new JwtResponseDTO(jwtUtil.generateJwtToken(login.getEmail()), login.getEmail());
	}

	@GetMapping("/userpro/refreshtoken")
	public JwtResponseDTO refreshtokenPro(HttpServletRequest request) {
		String jwt = "";
		String headerAuth = request.getHeader("Authorization");

		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
			jwt = headerAuth.substring(7, headerAuth.length());
		}

		String email = jwtUtil.parseUsername(jwt);
		return new JwtResponseDTO(jwtUtil.generateRefreshJwtToken(email), email);
	}

}
