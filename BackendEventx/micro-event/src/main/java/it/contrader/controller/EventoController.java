package it.contrader.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.contrader.dto.EventoDTO;
import it.contrader.dto.UserProDTO;
import it.contrader.service.EventoService;
import it.contrader.service.UserProService;
import it.contrader.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/micro-event")
public class EventoController extends AbstractController<EventoDTO> {

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private EventoService eventoService;
	@Autowired
	private UserProService userProService;


	@PostMapping("/userpro/evento/crea")
	public EventoDTO creazioneEvento(HttpServletRequest request, @RequestBody EventoDTO eventoDTO) {
		String jwt = this.parseJwt(request);
		EventoDTO evento = new EventoDTO();
		if (jwt != null && jwtUtil.validateJwtToken(jwt)) {
			String email = jwtUtil.parseUsername(jwt);
			UserProDTO userPro = userProService.findUserProByEmail(email);

			log.debug("TYPE: " + userPro.getUserType());
			if (userPro.getUserType().equals(userPro.getUserType().ESPOSITORE)){
				return null;
			}
			
			evento.setNomeEvento(eventoDTO.getNomeEvento());
			evento.setTipo(eventoDTO.getTipo());
			evento.setLuogo(eventoDTO.getLuogo());
			evento.setDataInizio(eventoDTO.getDataInizio());
			evento.setDataFine(eventoDTO.getDataFine());
			evento.setConEspositore(eventoDTO.getConEspositore());
			evento.setStalliDisponibili(eventoDTO.getStalliDisponibili());
			evento.setSponsorizzato(eventoDTO.getSponsorizzato());
			evento.setDescrizione(eventoDTO.getDescrizione());
			EventoDTO eventoFiltratoDTO = eventoService.findEventoByLuogoAndNomeEvento(evento.getLuogo(), evento.getNomeEvento());
			if(eventoFiltratoDTO != null) {
				return null;
			}
			eventoService.insert(evento);

		}
		return evento;
	}

	private String parseJwt(HttpServletRequest request) {
		String headerAuth = request.getHeader("Authorization");

		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
			return headerAuth.substring(7, headerAuth.length());
		}

		return null;
	}
	
	@PostMapping("/userpro/evento/update")
	public EventoDTO updateEvento(
			@RequestParam("id")Long id,
			@RequestParam("nomeEvento") String nomeEvento,
			@RequestParam("luogo") String luogo,
			@RequestParam("dataInizio") String dataInizio,
			@RequestParam("dataFine") String dataFine,
			@RequestParam("stalliDisponibili") Integer stalliDisponibili) {
	
		EventoDTO updateDTO = new EventoDTO();
		//updateDTO=eventoService.findEventoByLuogoAndNomeEvento(luogo , nomeEvento);
		updateDTO=eventoService.findEventoById(id);
		//log.debug("EVENTO TROVATO: " + updateDTO.getNomeEvento() + updateDTO.getLuogo());
		
		if(updateDTO != null) {
			
			 updateDTO.setLuogo(luogo);
			 updateDTO.setNomeEvento(nomeEvento);
			 updateDTO.setDataInizio(LocalDateTime.parse(dataInizio, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
			 updateDTO.setDataFine(LocalDateTime.parse(dataFine, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
			 updateDTO.setStalliDisponibili(stalliDisponibili);

			eventoService.update(updateDTO);
			
		}
			return updateDTO;
	}
	    
}
