package it.contrader.controller;

//import javax.servlet.http.HttpServletRequest;

//import org.apache.catalina.startup.SetAllPropertiesRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.contrader.dto.RegisterDTO;
import it.contrader.dto.UserDTO;
import it.contrader.service.UserService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/micro-event/user")
public class UserController extends AbstractController<UserDTO>{
	
	@Autowired
	private UserService userService;
	
	@PostMapping(value = "/register")
	public UserDTO register( @RequestBody RegisterDTO registerDTO ) {
		
		UserDTO userDTO = null;

		userDTO = userService.findUserByEmail(registerDTO.getEmail());
		
		if(userDTO != null) {
			
			return null;
		}
		
		userDTO = new UserDTO();
		
		userDTO.setNome(registerDTO.getNome());
		
		userDTO.setCognome(registerDTO.getCognome());
		
		userDTO.setEmail(registerDTO.getEmail());
		
		userDTO.setPassword(registerDTO.getPassword());
		
		userDTO = userService.insert(userDTO);
		
		return userDTO;
	
	}
	//POST Angular a UserDTO
/*	@PostMapping(value = "/register")
	public UserDTO register( @RequestBody UserDTO userDTO ) {
		//UserDTO user = new UserDTO(userDTO.getId(),userDTO.getPassword(), userDTO.getEmail());
		UserDTO registrato = userService.findUserByEmail(userDTO.getEmail());
		if(registrato != null) {
			System.out.println("UTENTE ESISTENTE: " + registrato.getEmail());
		}
		userService.insert(userDTO);
		log.error("UTENTE REGISTRATO");
		return userDTO;
	}*/
}
