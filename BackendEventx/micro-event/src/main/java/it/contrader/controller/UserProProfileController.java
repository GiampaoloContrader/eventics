package it.contrader.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.contrader.dto.UserProDTO;
import it.contrader.service.UserProService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("micro-event/userpro/profile") //end point created and mapped too
public class UserProProfileController {

    @Autowired
	UserProService userProService;
    
   // @Autowired
   // UserProDTO userProDTO;
    
	@PostMapping("/updateuserpro")
	public UserProDTO updateUtente(
			@RequestParam("id") Long idUserPro,
			@RequestParam("nome") String nome,
			@RequestParam("cognome") String cognome)
	
	{
		UserProDTO updateDTO = new UserProDTO();
		updateDTO=userProService.findUserProById(idUserPro);
		log.debug("UTENTE TROVATO: " + updateDTO.getEmail());
		
		 updateDTO.setNome(nome);
		 updateDTO.setCognome(cognome);
		 
		userProService.update(updateDTO);
		return updateDTO;
	}
	
	/*@PostMapping(value = "/updateuserpro")
	public UserProDTO updateUtente(@RequestBody ProfileDTO profileDTO) {  mi servirà questo metodo quando collegherò il frontend
		UserProDTO userProDTO = null;

		userProDTO = userProService.findUserProByEmail(profileDTO.getEmail());
		
		if(userProDTO != null) {
			
			return null;
		}

		UserProDTO updateDTO = new UserProDTO();
		//updateDTO=userProService.read(idUserpro);
		updateDTO.setNome(profileDTO.getNome());
		updateDTO.setCognome(profileDTO.getCognome());
		updateDTO.setCodiceFiscale(profileDTO.getCodiceFiscale());
		updateDTO.setEmail(profileDTO.getEmail());
		updateDTO.setPassword(profileDTO.getPassword());
		userProService.update(updateDTO);
		return updateDTO;
	}*/
}
