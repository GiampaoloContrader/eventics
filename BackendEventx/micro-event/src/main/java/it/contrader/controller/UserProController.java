package it.contrader.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.contrader.dto.LoginDTO;
import it.contrader.dto.RegisterDTO;
import it.contrader.dto.RegisterProDTO;
import it.contrader.dto.UserDTO;
import it.contrader.dto.UserProDTO;
import it.contrader.service.UserProService;


@RestController
@RequestMapping("/micro-event/userpro")
public class UserProController extends AbstractController<UserProDTO>{
	
	@Autowired
	private UserProService userProService;
	
	/*@PostMapping(value = "/register")
	public UserProDTO login( @RequestBody LoginDTO loginDTO ) {
	return userProService.findUserProByEmailAndPassword(loginDTO.getEmail(), loginDTO.getPassword());
	}*/
	
	@PostMapping(value = "/register")
	public UserProDTO register( @RequestBody RegisterProDTO registerProDTO ) {
		
		UserProDTO userProDTO = null;

		userProDTO = userProService.findUserProByEmail(registerProDTO.getEmail());
		
		if(userProDTO != null) {
			
			return null;
		}
		
		userProDTO = new UserProDTO();
		
		userProDTO.setNome(registerProDTO.getNome());
		
		userProDTO.setCognome(registerProDTO.getCognome());
		
		userProDTO.setCodiceFiscale(registerProDTO.getCodiceFiscale());
		
		userProDTO.setEmail(registerProDTO.getEmail());
		
		userProDTO.setPassword(registerProDTO.getPassword());
		
		userProDTO = userProService.insert(userProDTO);
		
		return userProDTO;
	
	}


}
