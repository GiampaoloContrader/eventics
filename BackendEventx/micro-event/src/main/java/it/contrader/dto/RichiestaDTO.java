package it.contrader.dto;

import it.contrader.model.Evento;
import it.contrader.model.Richiesta.Stato;
import it.contrader.model.UserPro;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RichiestaDTO {
	
	private Long id;
	private Stato stato;
	private EventoDTO idEvento;
	private UserProDTO idUserPro;
}