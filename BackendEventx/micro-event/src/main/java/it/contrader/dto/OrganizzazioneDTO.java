package it.contrader.dto;

import it.contrader.model.Evento;
import it.contrader.model.UserPro;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrganizzazioneDTO {
	
	private Long id;
	private EventoDTO idEvento;       //corretto cn dto
	private UserProDTO idUserPro;     //corretto con dto

}
