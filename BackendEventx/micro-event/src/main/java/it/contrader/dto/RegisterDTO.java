package it.contrader.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterDTO {
	
	private String nome;
	
	private String cognome;
	
	private String email;
	
	private String password;
	
	private String passwordConfirm;
	
}
