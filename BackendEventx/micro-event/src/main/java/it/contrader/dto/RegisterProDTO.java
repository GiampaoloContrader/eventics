package it.contrader.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterProDTO {
	
	private String nome;
	
	private String cognome;
	
	private String codiceFiscale;
	
	private String email;
	
	private String password;

}
