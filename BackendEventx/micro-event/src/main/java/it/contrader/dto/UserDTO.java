package it.contrader.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDTO {
	
	private Long id;
	private String email;
	private String password;
	private String nome;
	private String cognome;
	private String fotoProfilo;
	private Boolean notificheEventiSeguiti;
	private Boolean notificheNuoviEventi;
	private Boolean notificheDagliEspositori;
	private Boolean notificheDagliOrganizzatori;

}
