package it.contrader.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventiSeguitiDTO {

	private Long Id;
	private UserDTO  idUtente;
	private EventoDTO  idEvento;
	
	//private int idUtente;
	//private int idEvento;
}
