package it.contrader.dto;

import java.time.LocalDate;

import it.contrader.model.UserPro.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserProDTO {
	
	private Long id;	
	private String email;
	private String password;
	private UserType userType;
	private String nome;
	private String cognome;
	private String codiceFiscale;
	private String sesso;
	private LocalDate dataDiNascita;
	private String numeroDiTelefono;
	private String residenza;
	private String domicilio;
	private String partitaIVA;
	private String pec;
	private String cittadinanza;
	private String immagineCartaIdentita;
	private String immagineAbilitazione;
}
