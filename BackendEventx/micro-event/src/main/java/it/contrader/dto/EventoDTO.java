package it.contrader.dto;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.persistence.Transient;

import it.contrader.model.UserPro.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventoDTO {
	private Long id;
	private String nomeEvento;
	private String tipo;
	private String luogo;
	private LocalDateTime dataInizio;
	private LocalDateTime dataFine;
	private Boolean conEspositore;
	private Integer stalliDisponibili;
	private Boolean sponsorizzato;
	private String descrizione;
	


}
