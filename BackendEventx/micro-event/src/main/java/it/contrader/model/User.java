package it.contrader.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true)
	private String email;
	private String password;
	private String nome;
	private String cognome;
	private String fotoProfilo;
	private Boolean notificheEventiSeguiti;
	private Boolean notificheNuoviEventi;
	private Boolean notificheDagliEspositori;
	private Boolean notificheDagliOrganizzatori;
}
