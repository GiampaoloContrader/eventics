package it.contrader.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserProSecurity implements UserDetails {

	private static final long serialVersionUID = 1L;
	
	private final UserPro userPro;
	
	public UserProSecurity(UserPro userPro) {
		this.userPro = userPro;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return this.userPro.getPassword();
	}

	@Override
	public String getUsername() {
		return this.userPro.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
