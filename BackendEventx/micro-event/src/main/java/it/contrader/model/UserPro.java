package it.contrader.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserPro {
	
	public enum UserType{
		
		ORGANIZZATORE,ESPOSITORE
	}
	
	//da implementare sucessivamente Enum per lo SPID!!!!
	 
 	@Id
 	@GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
 	
 	@Column(unique = true)
     private String email;
     private String password;
     private UserType userType;
     private String nome;
 	 private String cognome;
 	 private String codiceFiscale;
 	 private String sesso;
 	 private LocalDate dataDiNascita;
 	 private String numeroDiTelefono;
 	 private String residenza;
 	 private String domicilio;
 	 private String partitaIVA;
 	 private String pec;
 	 private String cittadinanza;
 	 private String immagineCartaIdentita;
 	 private String immagineAbilitazione;
     
    //@OneToOne()
    //@JoinColumn(name = "ProfiloPro", referencedColumnName = "id")
    //private ProfiloPro profilo;
    

}
