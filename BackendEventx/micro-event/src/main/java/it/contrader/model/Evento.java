package it.contrader.model;
//import the necessary libraries

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import it.contrader.dto.UserProDTO;
import it.contrader.model.UserPro.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//the annotations to be integrated


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Evento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	/*Event è unico perché ci permette di non limitare i numeri di espositori/organizatori presenti*/
	private String nomeEvento;
	private String tipo;
	private String luogo;
	private LocalDateTime dataInizio;
	private LocalDateTime dataFine;
	private Boolean conEspositore;
	private Integer stalliDisponibili;
	private Boolean sponsorizzato;
	private String descrizione;
	
}
