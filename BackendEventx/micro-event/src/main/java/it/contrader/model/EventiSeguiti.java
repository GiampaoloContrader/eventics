package it.contrader.model;
//import the necessary libraries




import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class EventiSeguiti {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @ManyToOne
  //@OneToMany()
  @JoinColumn(name = "idUser")
  private User user;
  
  
  /*relazione con evento*/
  @ManyToOne
  //@OneToMany()
  @JoinColumn(name = "idEvento")
  private Evento evento;
  
}