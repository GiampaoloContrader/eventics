package it.contrader.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Richiesta {
	
public enum Stato{
		PRENOTABILE, ATTESA, COMPLETAMENTO, ACCETTATA, RIFIUTATA
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Stato stato;
	
	@ManyToOne
    @JoinColumn(name = "idEvento")
	private Evento idEvento;
	
	@ManyToOne
    @JoinColumn(name = "idUserPro")
	private UserPro idUserPro;

}