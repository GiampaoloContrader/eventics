package it.contrader.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.contrader.model.Evento;
import it.contrader.model.Richiesta;
import it.contrader.model.Richiesta.Stato;
import it.contrader.model.UserPro;

@Repository
@Transactional
public interface RichiestaRepository extends CrudRepository <Richiesta,Long>{
	
	Richiesta findRichiestaByIdEvento(Evento idEvento);
	Richiesta findRichiestaByIdUserPro(UserPro idUserPro);
	Richiesta findRichiestaByStato(Stato stato);
	
}
