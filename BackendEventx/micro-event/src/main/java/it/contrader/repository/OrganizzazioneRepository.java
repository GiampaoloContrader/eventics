package it.contrader.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.contrader.dto.EventoDTO;
import it.contrader.dto.OrganizzazioneDTO;
import it.contrader.dto.UserProDTO;
import it.contrader.model.Evento;
import it.contrader.model.Organizzazione;
import it.contrader.model.UserPro;

@Repository
@Transactional
public interface OrganizzazioneRepository extends CrudRepository <Organizzazione,Long>{   
	
	//Organizzazione findOrganizzazioneByIdEventoDTO(EventoDTO idEvento);           //corretto con dto
	//Organizzazione findOrganizzazioneByIdUserProDTO(UserProDTO idUserPro);        //corretto con dto
	
	Organizzazione findOrganizzazioneByIdEvento(Evento idEvento);           
	Organizzazione findOrganizzazioneByIdUserPro(UserPro idUserPro);        
}
