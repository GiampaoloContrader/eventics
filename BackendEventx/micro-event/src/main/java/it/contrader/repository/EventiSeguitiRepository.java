package it.contrader.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.contrader.model.EventiSeguiti;


@Repository
@Transactional

public interface EventiSeguitiRepository extends CrudRepository<EventiSeguiti,Long> {
	
	EventiSeguiti findEventiSeguitiById(long id);

}
