package it.contrader.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.contrader.model.User;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Long> {
	User findUserByEmailAndPassword(String email, String password);
	User findUserByEmail(String email);
	User findUserByNomeAndCognome(String nome, String cognome);
	User findUserById(Long id);
}
