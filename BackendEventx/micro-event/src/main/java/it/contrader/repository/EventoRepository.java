package it.contrader.repository;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.contrader.model.Evento;

@Repository
@Transactional
public interface EventoRepository extends CrudRepository<Evento,Long> {
	Evento findEventoByNomeEvento(String nomeEvento);
	Evento findEventoByTipo(String tipo);
	Evento findEventoByLuogo(String luogo);
	Evento findEventoByLuogoAndNomeEvento(String luogo,String nomeEvento);
	Evento findEventoByDataInizio(LocalDateTime dataInizio);
	Evento findEventoBySponsorizzato(Boolean sponsorizzato);
	Evento findEventoById(Long id);
	}
