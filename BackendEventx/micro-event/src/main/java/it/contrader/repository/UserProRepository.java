package it.contrader.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.contrader.model.UserPro;
import it.contrader.model.UserPro.UserType;

@Repository
@Transactional
public interface UserProRepository extends CrudRepository <UserPro,Long> {
	
	UserPro findUserProByEmailAndPassword(String email, String password);
    UserPro findUserProByUserType(UserType userType);
    UserPro findUserProByEmail(String email);
    UserPro findUserProByNomeAndCognome(String nome, String cognome);
    UserPro findUserProById(Long id);
}
