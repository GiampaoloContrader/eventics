package it.contrader.converter;

import org.springframework.stereotype.Component;

import it.contrader.dto.UserProDTO;
import it.contrader.model.UserPro;

@Component
public class UserProConverter extends AbstractConverter<UserPro,UserProDTO>{
	
	@Override
	public UserPro toEntity(UserProDTO userProDTO) {
		
		UserPro userPro = null;
		
		if (userProDTO != null) {
			
			userPro = new UserPro(userProDTO.getId(), userProDTO.getEmail(), userProDTO.getPassword(),  
					userProDTO.getUserType(), userProDTO.getNome(), userProDTO.getCognome(), 
					userProDTO.getCodiceFiscale(), userProDTO.getSesso(), userProDTO.getDataDiNascita(), 
					userProDTO.getNumeroDiTelefono(), userProDTO.getResidenza(), userProDTO.getDomicilio(), 
					userProDTO.getPartitaIVA(), userProDTO.getPec(), userProDTO.getCittadinanza(), 
					userProDTO.getImmagineCartaIdentita(), userProDTO.getImmagineAbilitazione());
					}
		return userPro;
	}
	
	@Override
	public UserProDTO toDTO(UserPro userPro) {
		
		UserProDTO userProDTO = null;
		
		if(userPro != null) {
			
			userProDTO = new UserProDTO(userPro.getId(), userPro.getEmail(), userPro.getPassword(),  
					userPro.getUserType(), userPro.getNome(), userPro.getCognome(), 
					userPro.getCodiceFiscale(), userPro.getSesso(), userPro.getDataDiNascita(), 
					userPro.getNumeroDiTelefono(), userPro.getResidenza(), userPro.getDomicilio(), 
					userPro.getPartitaIVA(), userPro.getPec(), userPro.getCittadinanza(), 
					userPro.getImmagineCartaIdentita(), userPro.getImmagineAbilitazione());
		}
		
		return userProDTO;
	}
	
}
