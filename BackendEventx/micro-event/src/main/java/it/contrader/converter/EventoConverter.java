package it.contrader.converter;

import org.springframework.stereotype.Component;

import it.contrader.dto.EventoDTO;
import it.contrader.model.Evento;

@Component
public class EventoConverter extends AbstractConverter<Evento, EventoDTO> {
	@Override
	public Evento toEntity(EventoDTO eventoDTO) {

		Evento evento = null;

		if (eventoDTO != null) {

			evento = new Evento(eventoDTO.getId(), eventoDTO.getNomeEvento(), eventoDTO.getTipo(), eventoDTO.getLuogo(),
					eventoDTO.getDataInizio(), eventoDTO.getDataFine(), eventoDTO.getConEspositore(),
					eventoDTO.getStalliDisponibili(), eventoDTO.getSponsorizzato(), eventoDTO.getDescrizione());
		}
		return evento;
	}

	@Override
	public EventoDTO toDTO(Evento evento) {

		EventoDTO eventoDTO = null;

		if (evento != null) {

			eventoDTO = new EventoDTO(evento.getId(), evento.getNomeEvento(), evento.getTipo(), evento.getLuogo(),
					evento.getDataInizio(), evento.getDataFine(), evento.getConEspositore(),
					evento.getStalliDisponibili(), evento.getSponsorizzato(), evento.getDescrizione());

		}
		return eventoDTO;
	}

}
