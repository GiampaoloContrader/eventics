package it.contrader.converter;

import org.springframework.stereotype.Component;

import it.contrader.dto.UserDTO;
import it.contrader.model.User;

@Component
public class UserConverter extends AbstractConverter<User,UserDTO>{
	
	@Override
	public User toEntity(UserDTO userDTO) {
		
		User user = null;
		
		if (userDTO != null) {
			
			user = new User(userDTO.getId(), userDTO.getEmail(),  userDTO.getPassword(), userDTO.getNome(), userDTO.getCognome(), userDTO.getFotoProfilo(), userDTO.getNotificheEventiSeguiti(), userDTO.getNotificheNuoviEventi(), userDTO.getNotificheDagliEspositori(), userDTO.getNotificheDagliOrganizzatori());
		}
		return user;
	}
	
	@Override
	public UserDTO toDTO(User user) {
		
		UserDTO userDTO = null;
		
		if(user != null) {
			
			userDTO = new UserDTO(user.getId(), user.getEmail(), user.getPassword(), user.getNome(), user.getCognome(), user.getFotoProfilo(), user.getNotificheEventiSeguiti(), user.getNotificheNuoviEventi(), user.getNotificheDagliEspositori(), user.getNotificheDagliOrganizzatori());
		}
		
		return userDTO;
	}
	
}
