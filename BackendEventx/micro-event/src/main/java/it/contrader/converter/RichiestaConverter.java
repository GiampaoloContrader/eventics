package it.contrader.converter;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import it.contrader.dto.RichiestaDTO;
import it.contrader.model.Richiesta;

@Component
public class RichiestaConverter extends AbstractConverter<Richiesta,RichiestaDTO>{
	
	@Autowired
	EventoConverter eventoConverter;
	
	@Autowired
	UserProConverter userProConverter;


	@Override
	public Richiesta toEntity(RichiestaDTO richiestaDTO) {
		
		Richiesta richiesta = null;
		
		if(richiestaDTO != null) {
			
			richiesta = new Richiesta (richiestaDTO.getId(),richiestaDTO.getStato(), eventoConverter.toEntity(richiestaDTO.getIdEvento()), userProConverter.toEntity(richiestaDTO.getIdUserPro()));
		}
		return richiesta;
	}

	@Override
	public RichiestaDTO toDTO(Richiesta richiesta) {
		
		RichiestaDTO richiestaDTO = null;
		
		if(richiesta != null) {
			
			richiestaDTO = new RichiestaDTO(richiesta.getId(), richiesta.getStato(), eventoConverter.toDTO(richiesta.getIdEvento()), userProConverter.toDTO(richiesta.getIdUserPro()));
		}
		return richiestaDTO;
	}

}




