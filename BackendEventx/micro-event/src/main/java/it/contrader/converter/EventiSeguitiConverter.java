package it.contrader.converter;

import org.springframework.beans.factory.annotation.Autowired;
import  org.springframework.stereotype.Component;
import it.contrader.dto.EventiSeguitiDTO;
import it.contrader.model.EventiSeguiti;
@Component

public class EventiSeguitiConverter extends AbstractConverter<EventiSeguiti , EventiSeguitiDTO> {
	
	@Autowired
	UserConverter userconverter;
	
	@Autowired
	EventoConverter eventoconverter;
	
	
	@Override
	public EventiSeguiti toEntity(EventiSeguitiDTO eventiseguitiDTO) {
		
		
		
		
		EventiSeguiti eventiseguiti = null;
		
		if (eventiseguitiDTO != null) {
			
			eventiseguiti = new EventiSeguiti(eventiseguitiDTO.getId(), userconverter.toEntity(eventiseguitiDTO.getIdUtente()), eventoconverter.toEntity(eventiseguitiDTO.getIdEvento()));
		}
		return eventiseguiti;
	}
	
	@Override
	public EventiSeguitiDTO toDTO(EventiSeguiti eventiseguiti) {
		
		EventiSeguitiDTO eventiseguitiDTO = null;
		
		if(eventiseguiti != null) {
			
		eventiseguitiDTO = new EventiSeguitiDTO(eventiseguiti.getId(), userconverter.toDTO(eventiseguiti.getUser()),eventoconverter.toDTO(eventiseguiti.getEvento()));
		
		}
		return eventiseguitiDTO;
	}
	
}