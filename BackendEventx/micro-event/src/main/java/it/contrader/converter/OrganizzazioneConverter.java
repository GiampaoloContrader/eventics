package it.contrader.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.contrader.dto.OrganizzazioneDTO;
import it.contrader.model.Organizzazione;

@Component
public class OrganizzazioneConverter extends AbstractConverter<Organizzazione,OrganizzazioneDTO>{

	@Autowired
    EventoConverter eventoConverter;
	
	@Autowired
	UserProConverter userProConverter;
	
	@Override
	public Organizzazione toEntity(OrganizzazioneDTO organizzazioneDTO) {
		
		Organizzazione organizzazione = null;
		
		if(organizzazioneDTO != null) {
			
			organizzazione = new Organizzazione (organizzazioneDTO.getId(),eventoConverter.toEntity(organizzazioneDTO.getIdEvento()),
					userProConverter.toEntity(organizzazioneDTO.getIdUserPro()));
			//organizzazione = new Organizzazione (organizzazioneDTO.getIdEvento(),organizzazioneDTO.getIdUserPro());
		}
		return organizzazione;
	}

	@Override
	public OrganizzazioneDTO toDTO(Organizzazione organizzazione) {
		
		OrganizzazioneDTO organizzazioneDTO = null;
		
		if(organizzazione != null) {
			
			organizzazioneDTO = new OrganizzazioneDTO(organizzazione.getId(),eventoConverter.toDTO(organizzazione.getIdEvento()),
					userProConverter.toDTO(organizzazione.getIdUserPro()));
		}
		return organizzazioneDTO;
	}

}
